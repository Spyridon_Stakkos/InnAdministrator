﻿CREATE TABLE [dbo].[Items] (
    [Id]      UNIQUEIDENTIFIER NOT NULL,
    [Name]    NCHAR (100)      NOT NULL,
    [SellIn]  INT              NOT NULL,
    [Quality] INT              NOT NULL,
    CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED ([Id] ASC)
);

