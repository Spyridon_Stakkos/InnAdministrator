﻿CREATE TABLE [dbo].[ItemProperties] (
    [Id]     UNIQUEIDENTIFIER NOT NULL,
    [ItemId] UNIQUEIDENTIFIER NOT NULL,
    [Name]   NCHAR (100)      NOT NULL,
    [Value]  NCHAR (100)      NOT NULL,
    CONSTRAINT [PK_ItemProperties] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ItemProperties_Items] FOREIGN KEY ([ItemId]) REFERENCES [dbo].[Items] ([Id])
);

