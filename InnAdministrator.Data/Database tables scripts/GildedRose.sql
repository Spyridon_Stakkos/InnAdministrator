USE [GildedRose]
GO
/****** Object:  Table [dbo].[ItemProperties]    Script Date: 1/24/2018 4:22:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ItemProperties](
	[Id] [uniqueidentifier] NOT NULL,
	[ItemId] [uniqueidentifier] NOT NULL,
	[Name] [nchar](100) NOT NULL,
	[Value] [nchar](100) NOT NULL,
 CONSTRAINT [PK_ItemProperties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Items]    Script Date: 1/24/2018 4:22:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Items](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nchar](100) NOT NULL,
	[SellIn] [int] NOT NULL,
	[Quality] [int] NOT NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[ItemProperties] ([Id], [ItemId], [Name], [Value]) VALUES (N'456a672e-9e56-4455-9e3d-24ca5c493fc8', N'aedd7132-2b02-4761-be69-205127697c16', N'ItemType                                                                                            ', N'Ordinary                                                                                            ')
INSERT [dbo].[ItemProperties] ([Id], [ItemId], [Name], [Value]) VALUES (N'6dd4b790-dd81-4556-95f8-50225ed6b7d3', N'ecd01d71-fe82-44d4-b577-1224fb8e24c4', N'ItemType                                                                                            ', N'AgedCheese                                                                                          ')
INSERT [dbo].[ItemProperties] ([Id], [ItemId], [Name], [Value]) VALUES (N'51dd0da8-f4d1-4b99-a827-9b492b35bb67', N'ae402e1e-f69f-4a80-bb5b-6f6029609557', N'ItemType                                                                                            ', N'Legendary                                                                                           ')
INSERT [dbo].[ItemProperties] ([Id], [ItemId], [Name], [Value]) VALUES (N'4d37f935-1579-4649-8a7b-c6d7074d75c6', N'fa372e4e-7095-4c56-96d1-3a263cfb260f', N'ItemType                                                                                            ', N'Conjured                                                                                            ')
INSERT [dbo].[Items] ([Id], [Name], [SellIn], [Quality]) VALUES (N'ecd01d71-fe82-44d4-b577-1224fb8e24c4', N'Aged Brie                                                                                           ', 2, 0)
INSERT [dbo].[Items] ([Id], [Name], [SellIn], [Quality]) VALUES (N'aedd7132-2b02-4761-be69-205127697c16', N'+5 Dexterity Vest                                                                                   ', 10, 20)
INSERT [dbo].[Items] ([Id], [Name], [SellIn], [Quality]) VALUES (N'fa372e4e-7095-4c56-96d1-3a263cfb260f', N'Conjured Mana Cake                                                                                  ', 3, 6)
INSERT [dbo].[Items] ([Id], [Name], [SellIn], [Quality]) VALUES (N'9f573726-96cc-4001-a29b-696429c5f8b1', N'Elixir of the Mongoose                                                                              ', 5, 7)
INSERT [dbo].[Items] ([Id], [Name], [SellIn], [Quality]) VALUES (N'ae402e1e-f69f-4a80-bb5b-6f6029609557', N'Sulfuras, Hand of Ragnaros                                                                          ', 0, 80)
INSERT [dbo].[Items] ([Id], [Name], [SellIn], [Quality]) VALUES (N'c14ed9c1-fd38-4dec-8330-82b6c0e8a140', N'Backstage passes to a TAFKAL80ETC concert                                                           ', 15, 20)
ALTER TABLE [dbo].[ItemProperties]  WITH CHECK ADD  CONSTRAINT [FK_ItemProperties_Items] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Items] ([Id])
GO
ALTER TABLE [dbo].[ItemProperties] CHECK CONSTRAINT [FK_ItemProperties_Items]
GO
