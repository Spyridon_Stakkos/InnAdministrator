﻿using InnAdministrator.Data.Context;
using InnAdministrator.Services;
using Xunit;

namespace GildedRose.Tests.Unit.TestFixtures
{
    public class InnAdministratorFixture
    {
        public GildedRoseAdministrator InnAdministrator { get; private set; }

        public InnAdministratorFixture()
        {
            InnAdministratorContext context = new InnAdministratorContext();
            GildedRoseDataService dataService = new GildedRoseDataService(context);
            InnAdministrator = new GildedRoseAdministrator(dataService);
        }
    }

    [CollectionDefinition("Gilded Rose tests")]
    public class GildedRoseTestsCollection : ICollectionFixture<InnAdministratorFixture>
    {
    }
}
