﻿using GildedRose.Tests.Unit.TestFixtures;
using Xunit;

namespace GildedRose.Tests.Unit.ItemTypeQualityCaclulationTests
{
    [Collection("Gilded Rose tests")]
    [Trait("ItemTypeQualityCalculation", "Legendary")]
    public class LegendaryItemTests
    {
        private readonly InnAdministratorFixture _innAdministratorFixture;
        
        public LegendaryItemTests(InnAdministratorFixture innAdministratorFixture)
        {
            _innAdministratorFixture = innAdministratorFixture;
        }

        [Fact]
        public void ShouldNeverHaveAQualityLessThanZero()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateLegendaryItemQuality(-1, 1);

            Assert.True(result == 0);
        }

        [Fact]
        public void ShouldNeverHaveItsQualityChanged()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateLegendaryItemQuality(80, 1);

            Assert.True(result == 80);
        }
    }
}
