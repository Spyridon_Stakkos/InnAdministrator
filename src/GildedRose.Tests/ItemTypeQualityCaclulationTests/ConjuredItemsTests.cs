﻿using GildedRose.Tests.Unit.TestFixtures;
using Xunit;

namespace GildedRose.Tests.Unit.ItemTypeQualityCaclulationTests
{
    [Collection("Gilded Rose tests")]
    [Trait("ItemTypeQualityCalculation", "Conjured")]
    public class ConjuredItemsTests
    {
        private readonly InnAdministratorFixture _innAdministratorFixture;
        
        public ConjuredItemsTests(InnAdministratorFixture innAdministratorFixture)
        {
            _innAdministratorFixture = innAdministratorFixture;
        }

        [Fact]
        public void ShouldNotHaveQualityLessThanZero()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(-1, 1);

            Assert.True(result == 0);
        }

        [Fact]
        public void QualityShouldDecreaseByTwoIfSellInHasNotPassed()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(5, 15);

            Assert.True(result == 3);
        }

        [Fact]
        public void QualityShouldDecreaseByFourIfSellInHasPassed()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(5, -1);

            Assert.True(result == 1);
        }

        [Fact]
        public void QualityShouldDecreaseByOnlyOneIfSellInHasNotPassedAndQualityIsOne()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(1, 1);

            Assert.True(result == 0);
        }

        [Fact]
        public void QualityShouldDecreaseByOnlyThreeIfSellInIsHasPassedAndQualityIsThree()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(3, -1);

            Assert.True(result == 0);
        }

        [Fact]
        public void QualityShouldNeverBeMoreThanFiftyRegardLessOfSellIn()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(55, -1);

            Assert.True(result == 50);
        }

        [Fact]
        public void QualityShouldDecreaseToFourtyNineIfSellInHasNotPassedAndQualityIsFiftyOne()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(51, 1);

            Assert.True(result == 49);
        }

        [Fact]
        public void QualityShouldDecreaseToFortyNineIfSellInHasPassedAndQualityIsFiftyThree()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateConjuredItemQuality(53, -1);

            Assert.True(result == 49);
        }
    }
}
