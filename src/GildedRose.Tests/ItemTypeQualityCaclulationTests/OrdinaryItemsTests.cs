﻿using GildedRose.Tests.Unit.TestFixtures;
using Xunit;

namespace GildedRose.Tests.Unit.ItemTypeQualityCaclulationTests
{
    [Collection("Gilded Rose tests")]
    [Trait("ItemTypeQualityCalculation", "Ordinary")]
    public class OrdinaryItemsTests
    {
        private readonly InnAdministratorFixture _innAdministratorFixture;

        public OrdinaryItemsTests(InnAdministratorFixture innAdministratorFixture)
        {
            _innAdministratorFixture = innAdministratorFixture;
        }

        [Fact]
        public void ShouldNotHaveQualityLessThanZero()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateOrdinaryItemQuality(-1, 15);

            Assert.True(result == 0);
        }

        [Fact]
        public void QualityShouldDecreaseByOneIfSellInHasNotPassed()
        {


            int result = _innAdministratorFixture.InnAdministrator.CalculateOrdinaryItemQuality(5, 15);

            Assert.True(result == 4);
        }

        [Fact]
        public void QualityShouldDecreaseByTwoIfSellInIHasPassedAndQualityIsGreaterThanOne()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateOrdinaryItemQuality(5, -1);

            Assert.True(result == 3);
        }

        [Fact]
        public void QualityShouldDecreaseByOnlyOneIfSellHasPassedAndQualityIsOne()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateOrdinaryItemQuality(1, -1);

            Assert.True(result == 0);
        }

        [Fact]
        public void QualityShouldNeverBeMoreThanFiftyRegardLessOfSellIn()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateOrdinaryItemQuality(55, -1);

            Assert.True(result == 50);
        }

        [Fact]
        public void QualityShouldDecreaseToNoMoreThanFiftyIfSellInHasNotPassed()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateOrdinaryItemQuality(51, 1);

            Assert.True(result == 50);
        }

        [Fact]
        public void QualityShouldDecreaseToFortyNineIfSellInHasPassedAndQualityIsFiftyOne()
        {
            int result = _innAdministratorFixture.InnAdministrator.CalculateOrdinaryItemQuality(51, -1);

            Assert.True(result == 49);
        }
    }
}
